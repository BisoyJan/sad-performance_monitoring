import Vue from "vue";
import VueRouter from "vue-router";
// import store from '../store'
import Home from "../components/pages/Home/Home";
import Aptitude from "../components/pages/Home/Aptitude";
import Login from "../components/pages/Login/Login";
import Signup from "../components/pages/Login/Signup";
import Welcome from "../components/pages/Login/Welcome";
import Summary from "../components/pages/Home/Attendance/Summary";
import Examination from "../components/pages/Home/Exam/Examination";
import ExamSummary from "../components/pages/Home/Exam/Summary";
import Settings from "../components/pages/Home/Settings";
import Students from "../components/pages/Home/Students";
import NotFound from "../components/pages/NotFound/NotFound";
import Dashboard from "../components/pages/Home/Dashboard";
import Attendance from "../components/pages/Home/Attendance/Attendance";

Vue.use(VueRouter);

const routes = [
	{
		path: "/",
		name: "Welcome",
		component: Welcome,
		meta: { hasUser: true },
	},
	{
		path: "/login",
		name: "Login",
		component: Login,
		meta: { hasUser: true },
	},
	{
		path: "/signup",
		name: "Create Account",
		component: Signup,
		meta: { hasUser: true },
	},
	{
		path: "/home",
		name: "Home",
		component: Home,
		children: [
			{
				path: "dashboard",
				name: "dashboard",
				components: {
					dashboard: Dashboard,
				},
			},
			{
				path: "attendance",
				name: "attendance",
				components: {
					attendance: Attendance,
				},
			},
			{
				path: "students",
				name: "students",
				components: {
					activities: Students,
				},
			},
			{
				path: "examination",
				name: "examination",
				components: {
					activities: Examination,
				},
			},
			{
				path: "aptitude",
				name: "aptitude",
				components: {
					activities: Aptitude,
				},
			},
			{
				path: "settings",
				name: "settings",
				components: {
					activities: Settings,
				},
			},
			{
				path: "/attendance/summary",
				name: "summary",
				components: {
					summary: Summary,
				},
			},
			{
				path: "/exam/summary",
				name: "examsummary",
				components: {
					examsummary: ExamSummary,
				},
			},
		],
		meta: { requiresLogin: true },
	},
	{
		path: "*",
		name: "Unkown",
		component: NotFound,
	},
];

const router = new VueRouter({
	mode: "history",
	routes,
});

router.beforeEach((to, from, next) => {
	if (
		to.matched.some((record) => record.meta.requiresLogin) &&
		!localStorage.getItem("auth")
	) {
		next({ name: "Login" });
	} else if (
		to.matched.some((record) => record.meta.hasUser) &&
		localStorage.getItem("auth")
	) {
		next({ name: "Home" });
	} else {
		next();
	}
});

export default router;
