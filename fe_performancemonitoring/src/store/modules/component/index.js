import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const component = 'component'

export default ({
  namespaced: true,
  state: {
    component: [],
  },
  actions: {
    async getComponents({commit}){
      const res = await AXIOS.get(`${component}`).then(response => {
        commit('SET_COMPONENT', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
  },
  getters: {
   getComponent(state){
     return state.data
   },
  },
  mutations: {
    SET_COMPONENT(state, data){
      state.component = data
    },
  },
})