import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const Attendance = 'attendance'
const attendance_summary = 'summary'
export default ({
  namespaced: true,
  state: {
    attendance: [],
    summary: [],
  },
  actions: {
    async getAttendance({commit}, data){
      const res = await AXIOS.get(`${Attendance}?section_id=${data.section_id}&date=${data.date}&semester_id=${data.semester_id}&school_year_id=${data.school_year_id}`).then(response => {
        commit('SET_ATTENDANCE', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async updateAttendance({commit}, {id, data}){
      const res = await AXIOS.put(`${Attendance}/${id}`, data).then(response => {
        return response
      }).catch(error => {
        return error.response
      });

      return res;
    },
  },
  getters: {
    getAttendance(state){
      return state.user;
    },
  },
  mutations: {
    SET_ATTENDANCE(state, data){
      state.attendance = data
    },
    SET_SUMMARY(state, data){
      state.summary = data
    },
   
  },
})