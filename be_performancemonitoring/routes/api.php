<?php

use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\AttendanceSummaryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ComponentController;
use App\Http\Controllers\ExaminationController;
use App\Http\Controllers\ExaminationTypeController;
use App\Http\Controllers\InstructorController;
use App\Http\Controllers\MeritController;
use App\Http\Controllers\OverAllSummaryController;
use App\Http\Controllers\SchoolYearController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\SemesterController;
use App\Http\Controllers\StudentController;
use App\Models\AttendanceSummary;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/store', [AuthController::class, 'store']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/me', [AuthController::class, 'me']);    
});

Route::apiResource('section', SectionController::class);
Route::apiResource('student', StudentController::class);
Route::apiResource('account', InstructorController::class)->only('store');
Route::get('examination/summary', [StudentController::class, 'exam_summary']);
Route::apiResource('attendance', AttendanceController::class)->only('index', 'update');
Route::apiResource('summary', AttendanceSummaryController::class)->only('index');
Route::get('overallsummary', [OverAllSummaryController::class, 'index']);
Route::apiResource('examination', ExaminationController::class)->only('index', 'store');
Route::apiResource('aptitude', MeritController::class)->only('index', 'update', 'store');

Route::get('/semester', [SemesterController::class, 'index']);
Route::get('/component', [ComponentController::class, 'index']);
Route::get('/schoolyear', [SchoolYearController::class, 'index']);
Route::get('/examinationtype', [ExaminationTypeController::class, 'index']);