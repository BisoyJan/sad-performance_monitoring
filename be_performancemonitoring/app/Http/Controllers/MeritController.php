<?php

namespace App\Http\Controllers;

use App\Models\Merit;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MeritController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function index(){
        $student = Student::with(['merit'])->where('instructor_id', Auth::id())
        ->where('section_id', Request()->section_id)
        ->where('semester_id', Request()->semester_id)
        ->where('school_year_id', Request()->school_year_id)
        ->orderBy('students.gender', 'desc')
        ->orderBy('students.last_name', 'asc')
        ->orderBy('students.first_name', 'asc')->get();

        return response()->json($student);
    }

    public function store(Request $request){
        $data = [
            'section_id' => $request->section_id,
            'semester_id' => $request->semester_id,
            'school_year_id' => $request->school_year_id,
            'student_id' => $request->student_id,
            'merit' => 100,
        ];

        Merit::create($data);

        return response()->json('Student record created sucessfully!', 200);
    }

    public function update(Request $request){
        $studentmerit = Merit::where('student_id', Request()->student_id)
        ->where('semester_id', Request()->semester_id)
        ->where('school_year_id', Request()->school_year_id)->first();

        $studentmerit->update(['merit' => $request->merit]);


        return response()->json(['msg' => 'Merit updated successfully!']);
    }

    
}
