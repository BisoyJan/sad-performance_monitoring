<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function index(){
        $data = Student::with([
            'component:id,component', 'section:id,section', 
            ])->where('instructor_id', Auth::id())
            ->where('section_id', Request()->section_id)
            ->where('semester_id', Request()->semester_id)
            ->where('school_year_id', Request()->school_year_id)
            ->orderBy('students.gender', 'desc')
            ->orderBy('students.last_name', 'asc')
            ->orderBy('students.first_name', 'asc')->get();

        return response()->json($data);
    }
    
    public function store(Request $request){
        $component = User::with(['instructor.component'])->find(Auth::id());

        $student = Student::create([
            'id' => $request->id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'component_id' => $component->instructor->component_id,
            'section_id' => $request->section_id,
            'gender' => $request->gender,
            'instructor_id' => Auth::id(),
            'semester_id' => $request->semester_id,
            'school_year_id' => $request->school_year_id
        ]);

        $data = Student::with(['component:id,component', 'section:id,section', 'instructor:id'])->where('id', $request->id)->where('instructor_id', Auth::id())->get();
        
        return response()->json($data);
    }

    public function update(Request $request, $id){
        try {
            $student = Student::where('id', $id)->firstOrFail();
            $student->update([
                'id' => $request->id,
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'section_id' => $request->section_id,
                'gender' => $request->gender,
                'instructor_id' => Auth::id(),
            ]);

            $updated = Student::with(['component:id,component', 'section:id,section', 'instructor:id'])->where('id', $id)->firstOrFail();
            return response()->json($updated);

        } catch(ModelNotFoundException $exception) {
            return response()->json(['message' => 'Section not found']);
        }
    }

    public function destroy($id){
        Student::destroy($id);
        return response()->json(['msg' => 'Section deleted successfully!']);
    }

    public function exam_summary(){
        $student = Student::with(['examination'])->where('instructor_id', Auth::id())
            ->where('section_id', Request()->section_id)
            ->where('semester_id', Request()->semester_id)
            ->where('school_year_id', Request()->school_year_id)
            ->orderBy('students.gender', 'desc')
            ->orderBy('students.last_name', 'asc')
            ->orderBy('students.first_name', 'asc')->get();

        return response()->json($student, 200);
    }
}
