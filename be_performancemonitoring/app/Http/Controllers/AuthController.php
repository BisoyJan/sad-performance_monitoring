<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['store', 'login']]);
    }

    public function me()
    {
        return response()->json(User::with(['instructor.component'])->find(Auth::id()));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'bail|required|email|unique:users',
            'password' => 'bail|required|min:6',
            'instructor_id' => 'bail|required|exists:instructors,id'
        ]);

        User::create([
            'instructor_id' => $request->instructor_id,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response()->json(['success' => 'Account created successfuly!'], 200);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function logout()
    {
        JWTAuth::invalidate(Request()->token);
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth()->user()
        ])->header('Authorization: Bearer ', $token);
    }

    public function payload()
    {
        return auth()->payload();
    }
}
